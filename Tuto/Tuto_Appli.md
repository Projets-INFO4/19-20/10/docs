<img src="images_tuto_appli/Mit_app_inventor.png" width="200"> <h1>Création d'une application avec AppInventor</h1>
 
La Nucleo Board est une carte qui, équipée de capteurs appropriés, permet de fournir de nombreuses informations, comme par exemple la température.
 
Cette carte est munie d'une fonction BLE (ou Bluetooth Low Energy) permettant de s'y connecter à distance. Le but de ce tutoriel est donc de créer une application permettant de récupérer ces informations ou d'intéragir avec la carte directement depuis notre smartphone.
 
Pour ce faire, nous utiliserons l'outil AppInventor, permettant de simplifier la création d'application notamment grâce à une interface graphique similaire à celle de Scratch.
 
 <br />
 
 <h2>1. Initilisation du projet</h2>
 
Dans un premier temps, nous allons créer un nouveau projet depuis l'application AppInventor et analyser l'interface de développement.
 
Vous pourrez accéder au site en [cliquant ici](https://appinventor.mit.edu/).
 
Pour démarrer un nouveau projet il suffit de cliquer sur *Create Apps!*.
 
<div align="center"><img src="images_tuto_appli/Fentre_1.png" width="700"></div>
 
Vous pourrez à partir de ce moment-là, changer de langue (en haut à droite) et passer à la langue française : notre tutoriel est fait à partir du site en français. 
Une fois votre compte créé vous pourrez cliquer sur *Commencer nouveau projet...*. Nous appellerons notre projet "MyApply".
 
L'interface de développement de AppInventor est séparée en 2 parties : la partie design et la partie code.

<div align="center"> <img src="images_tuto_appli/fenetre_design.png " width="400"><img src="images_tuto_appli/fenetre_code.png " width="400"> </div>
 
 
Vous pourrez passer d'une fenêtre à l'autre en cliquant sur les boutons *Designer* et *Blocs* situés en haut à droite de l'écran

<br />

<h2>2. Design de l'écran d'accueil</h2>
 
Dans un premier temps nous allons travailler sur le design de notre application. Nous allons devoir déterminer quels sont éléments (boutons, label, listes ...) nécessaires.
 
Sur le côté gauche, vous disposez de plusieurs onglets contenant toutes les fonctionnalités que vous pouvez apporter à votre application.
 
Pour ajouter un item à notre écran il suffit de le faire glisser sur le dessin de l'écran de téléphone (la fenêtre *Interface*). Vous pouvez voir ici le menu prinicpal de notre application : 
 
<p align = "center">
 <img src="images_tuto_appli/fenetre_home.png " width="500" >
 </p>

La mise en place en place des items se fait sous forme d'arbre. Il est important d'identifier les différentes zones et les différentes fonctionnalités qu'elles vont contenir. 
 
Ici, on peut voir que les items (la loupe et le texte) sont encadrés dans une zone. Ces zones sont aussi appelées des *dispositions*. Elles définissent un "cadre" dans lequel sont regroupées plusieurs fonctionnalités.
 
<li> Ouvrez donc l'onglet <i>Disposition</i> sur le côté gauche. </li>

Il faut à présent choisir un type d'arrangement pour nos items dans ces zones : arbitrairement nous choissirons un arrangement vertical. 
 
<li> Sélectionnez <i>Arrangement vertical</i> et faites le glisser sur l'écran du smartphone.</li>
 
 
<div align="center"><img src="images_tuto_appli/arrangement_vertical.png " width="700" ></div>
 
Pour pouvoir centrer notre arrangement vertical, nous avons cliqué sur *Screen1* (dans la partie *Composants*), puis nous avons définit l'arrangement horizontal et vertical à *Center*. 
 
 
On peut voir sur l'écran, 2 objets différents :
<li> des textes : ce sont des <i>labels</i>, on peut les retrouver sous l'onglet <i>Interface Utilisateur</i>
<li> une image : cette image est en réalité un bouton (<i>Bouton</i>) que l'on peut retrouver sous l'onglet <i>Interface Utilisateur</i> également</li>
 
Ces objets peuvent être améliorés en changeant leur couleur, ou leur taille par exemple, sous le menu qui se trouve à droite (*Propriétés*). C'est pour cela, par exemple, que le bouton du centre est représenté avec une image de loupe ou que certains textes sont en gras, ou plus gros. Vous pourrez les personnaliser à votre guise.
 
 <li>Faites donc glisser sur l'écran 3 <i>Labels</i> et un <i>Bouton</i> à l'intérieur de votre <i>Arrangement vertical</i>. Vous pouvez changer le texte de ces composants dans la partie <i>Texte</i> des <i>Properties</i>.</li>
 
Notre application va servir à se mettre en relation avec la carte via BLE. Une fois la géolocalisation et le Bluetooth de votre téléphone activés, nous allons analyser notre environnement et sélectionner parmi la liste des appareils disponibles, celui auquel nous voulons nous connecter. Nous avons donc besoin d'un *Sélectionneur de liste*. Celui-ci se trouve dans l'onglet *Interface Utilisateur*. 
 
**Attention :** afin d'obtenir le design souhaité, vous devez rendre cette liste non visible en décochant le champ *Visible* dans ses *Propriétés*.
 
Entre l'*Interface* et les *Propriétés* vous retrouverez une partie *Composants* qui vous montre l'arbre des widgets que vous avez ajouté à votre application. Voici à quoi votre arbre devrait ressembler :
 
 <p align = "center">
<img src="images_tuto_appli/component.png " width="300" >
</p>

Vous pouvez cliquer sur *Renommer* pour renommer vos composants comme sur l'image ci-dessus.

Une dernière partie très importante est la capacité de notre application à se connecter en bluetooth. Dans notre cas c'est une connexion BLE qui n'est pas disponible dans les fonctionnalités de base de AppInventor. 

En dessous de la partie *Palette* vous trouverez un onglet *Extension*. Il faut importer le fichier <a href="../Application/connexion_BLE.aia"
 download>connexion_BLE</a>

<li> Une fois importée vous devez la faire glisser dans l'<i>Interface</i>. Vous la verrez apparaitre sous le smartphone dans la catégorie <i>Non visible components</i>. </li>
<li>De même pour pouvoir gérer différentes alertes comme la détection d'appareil vous aurez besoin d'un <i>Notificateur</i> dans la catégorie <i>Interface Utilisateur</i>.</li>

Vous devriez maintenant obtenir le résultat présenté au début de cette partie. 

<br />

<h2> 3. Mise en place de la connexion</h2>

Pour faire notre application nous allons devoir associer un comportement à ces composants, suivant les actions de l'utilisateur.
 
Basculez donc maitenant dans la partie *Blocs*
 
 <p align = "center">
<img align="center" src="images_tuto_appli/fenetre_code.png " width="600" >
</p>

La partie *Blocs* contient tout le "code" dont vous aurez besoin. 

Vous retrouvez ici toutes les bases de la programmation, avec les opérateurs booléens (vrai, faux), les synthaxes de boucles, etc. Chaque catégorie est symbolisée par une couleur différente comme vous pouvez le voir dans le panneau à gauche de l'écran. Nous pouvons également modifier ou appeler des instructions sur nos différents composants (créés dans la partie *Design*) en cliquant sur le composant à modifier et en utilisant les différents méthodes proposées. 

 </br>
 
<h4>Démarrage de l'application</h4>

La première étape est d'initialiser notre écran d'accueil, c'est-à-dire de définir les comportements qui se produisent à l'ouverture de l'application. 
<li>Pour cela, il existe dans le composant <i>Screen1</i> la structure <code>quand Screen1 initialise</code>. Faites glisser sur l'écran cette structure.</li>

Pour nous aider à visualiser l'état de notre connexion bluetooth nous choisissons de changer le fond du label <code>Etat_connexion </code> suivant l'état de la connexion : 

<li> <span style="color: #FF0000">Rouge</span> : Pas de connexion
<li> <span style="color: #FFC800">Orange</span> : Connexion en cours
<li> <span style="color: #00FF00">Vert</span> : Connexion réussie </li>

Pour changer la structure d'un label il faut utiliser la structure <code>mettre ... à</code>.

<li>En cliquant sur le composant <code>Etat_connexion</code> nous pouvons donc selectionner la structure <code>mettre Etat_connexion Couleur de fond à</code> et choisir la couleur adéquate dans la partie <i>Couleurs</i> (ici le rouge). Faites glisser ce composant dans le bloc <code>quand Screen1 initialise</code>. </li>

De même en l'abscence de connexion, le label <code>Etat_connexion</code> doit contenir le texte "Aucune connexion". 

<li>Donc nous pouvons également nous servir de<code>mettre Etat_connexion Text à</code> en y ajoutant un champ d'écriture dans <i>Texte</i>.</li>

Vous devriez obtenir ceci : 

 <div align = "center">
<img align="center" src="images_tuto_appli/cnx_1.png " width="600" >
</div>

<br />

<h4>Initilialisation du bouton connexion</h4>

Nous allons maintenant gérer ce qu'il se passe quand on appuie sur le bouton *Connexion* (<code>quand Connexion Clic</code>). 

<li> Donc sélectionnez ce composant là et ajouter le sur l'écran de code.</li>

Celui-ci doit permettre d'activer la recherche d'appareil (<code>appeler BluetoothLE1 StartScanning</code>). 

<li> Imbriquez ce code là dans le précédent.</li>

Pour aider l'utilisateur à connaitre l'état de l'application, nous utiliserons le *Notificateur* pour préciser l'action en cours. 

 <li> Rajoutez enfin le bloc <code>appeler Notificateur1 Afficher Alerte notice</code> + <i>Texte</i> : "Recherche BLE en cours .." </code></li>

 <div align = "center">
<img align="center" src="images_tuto_appli/cnx_2.png " width="600" >
</div>

 </br>
 
 <h4>Recherche de périphériques</h4>

Nous devons maintenant définir ce que l'on doit faire quand des appareils ont été trouvés. 

<li>Faites glisser le bloc <code>quand BluetoothLE1 DeviceFound</code> dans la fenêtre. </li>

Il faut dans un premier temps arrêter la recherche de périphériques.

<li>Imbriquez le bloc <code>appeler BluetoothLE1 StopScanning</code> à l'intérieur du précédent.</li>

Dans le champ *DeviceFound* de notre BLE se trouve la liste des périphériques disponibles. Nous allons donc mettre à jour notre *Liste_BLE* avec les éléments trouvés par notre BLE qui sont caractérisés par leur nom (donc par une chaîne de caractère) :

<li>Ajoutez le bloc <code>mettre List_BLE Éléments de la chaine à BluetoothLE1 DeviceList </code>.</li>

Notre *label* <code>Etat_connexion</code> a aussi changé. Celui ci va maintenant nous donner le nombre de BLE trouvés qui est équivalent à la taille de la liste des éléments de *list_BLE_Devices*.

<li> Pour cela, ajoutez le bloc <code>mettre Etat_connexion Texte à</code>. </li>

**Attention :** ici, vous voulez rajouter du texte avec une information contenue dans un composant. Vous aurez donc besoin de joindre ces deux éléments. 

<li>Pour cela, utilisez la structure <code>joint</code> dans l'onglet <i>Texte</i></li>

Vous voulez donc joindre un texte et la longueur de la liste d'appareils trouvés. 

<ul>
	<li>Pour cela, ajoutez un champ <code>Texte</code> contenant "Nombre de BLE trouvés :" et imbriquez le à la première partie du bloc <code>joint</code>. </li>
	<li>Pour la deuxième partie de cette structure, nous allons utiliser la structure <code>taille de la liste</code> (dans <i>Listes</i>) et y ajouter la liste souhaitée (celle dont nous souhaitons connaître la taille) : <code>Liste_BLE Éléments</code></li>
	<li>Une fois ces changements effectués nous pouvons afficher la liste des appareils trouvés en ajoutant la structure <code>appeler Liste_BLE ouvrir</code>. </li>
</ul>

 <div align = "center">
<img align="center" src="images_tuto_appli/cnx_3.png " width="600" >
</div>


</br>

<h4>Choix du périphérique</h4>

Une fois la liste des appareils disponibles affichée, nous allons devoir sélectionner celui qui nous intéresse. 


<u> Initialisation de l'adresse</u>

L'adresse de ce périphérique sera stockée dans une variable (onglet orange *Variables* dans le panneau latéral). Il est donc important avant tout d'initialiser cette variable, de dire qu'elle existe et lui donner un nom.

<li>Pour cela, ajoutez le bloc <code>initialise global <i>nom</i> à</code> et modifiez le nom pour que notre variable s'appelle <i>Adresse</i>. </li>

Comme cette adresse n'a encore aucune valeur nous pouvons lui donner un champ de texte vide. 

<li>Imbriquez donc un champ <i>Texte</i> vide à cette structure.</li>

  <div align = "center">
<img align="center" src="images_tuto_appli/cnx_4.png " width="600" >
</div>
 
<u> Récupération de l'adresse</u>

Nous allons donc traiter ce qu'il se passe après avoir choisi votre l'appareil. 

<li>Pour cela, nous allons utiliser la structure <code>quand Liste_BLE Après prise</code>.</li>

Dans un premier temps nous allons changer la valeur de notre variable *Adresse* en lui affectant celle sélectionnée dans la liste.

<li>Imbriquez donc le bloc <code>mettre ... à</code> (qui se trouve dans la catégorie <i>Variables</i> puisque nous voulons modifier la valeur d'<i>Adresse</i>). Puis utiisez la structure <code>Liste_BLE Sélection</code> pour mettre à jour notre variable avec l'appareil sélectionné depuis la liste.</li>

<u> Changement d'indicateur</u>

Nous allons ensuite changer notre témoin de connexion pour informer l'utilisateur du changement d'état de l'application. 

<li>Comme nous l'avons fait précedemment, nous allons changer le <code>Texte</code> du label <code>Etat_connexion</code>.</li> 

Comme précédemment nous allons vouloir signifier à quel périphérique nous souhaitons nous connecter. Pour cela, nous allons utiliser des blocs <code>joint</code> .

<li>Ajoutez donc un bloc joint qui sera composé d'un <i>Texte</i> : "Connexion à ", suivi de l'adresse de notre périphérique stockée dans notre variable <i>Adresse</i> récupérée grâce à la structure <code>obtenir global Adresse</code>.</li>

Or, la connexion n'étant pas encore terminé, nous allons préciser qu'elle est en cours.

<li>Rajoutez pour cela, une structure <code>joint</code> qui associera le contenu de notre variable <i>Adresse</i> à un <i>Texte</i> contenant " en cours". </li>

Nous changerons ensuite la couleur du fond en la passant à jaune.

<li>Utilisez donc la structure <code>mettre Etat_connexion Couleur de fond à</code> et y clipser la couleur <i>Orange</i>.</li>

<u> Connexion au périphérique</u>

Une fois la carté sélectionnée vous devez établir la connexion entre celle-ci et l'application.

<li>Pour cela nous utiliserons la structure <code> appeler BluetoothLE1 Se connecter index </code>. </li>

L'index représente le numéro du périphérique dans notre liste d'appareils disponibles. C'est donc celui ayant le nom de notre *Adresse* dans les éléments de la List_BLE. 

<li>Il faut donc ajouter la strucure <code>index dans la liste</code> qui combinera l'adresse du périphérique, donc notre variable <i>Adresse</i> (récupérée grâce au bloc <code>obtenir</code>) et <code>Liste_BLE Éléments</code> .</li>

<div align="center"><img align="center" src="images_tuto_appli/cnx_5.png " width="600" ></div>

<br />

Vous devriez finalement obtenir un code comme celui ci :

<p align = "center"> <img align="center" src="images_tuto_appli/code_cnx.png " width="600" > </p>


<br />

 <h2>4. Design de l'écran de contrôle</h2>
 
Comme pour l’écran de connexion, nous devons définir quels composants seront nécessaires à l’allumage de la LED et la prise de température. 

Nous souhaitons obtenir le résultat suivant : 

<p align = "center"> <img align="center" src="images_tuto_appli/deuxieme_ecran.png " width="300" > </p>

Pour cela, nous allons avoir besoin d’un label pour signifier à l’utilisateur que la connexion est établie, un bouton switch qui nous permettra d’allumer et d’éteindre la LED, un label pour afficher « Température : » (par pur esthétisme) ainsi qu’un dernier label qui servira à afficher la température courante. 

Nous allons néanmoins réutiliser le label *Etat_connection* créé précédemment pour afficher la réussite de la connexion avec la carte. 

<li>Nous allons utiliser un <i>Arrangement vertical</i> pour stocker tous ces éléments (hormis le label <i>Etat_connexion</i>), que nous allons ajoutez sous l'arrangement <i>Arrangement_connexion</i>. </li>

<li>Nous allons maintenant glisser un <i>Switch</i> dans cet <i>Arrangement vertical</i>, puis y rajouter un <i>Arrangement horizontal</i>.</li>

<li>Dans ce dernier, nous allons y insérer deux <i>Labels</i> : l’un contenant "Température : " et l’autre contenant la température en temps réel. </li>

<li>De plus, nous allons ajouter un <i>Bouton</i> pour nous déconnecter de la carte.</li>

Après avoir rajouté ces éléments, vous devriez obtenir cecie : 

<p align = "center"> <img align="center" src="images_tuto_appli/component2.png " width="700" > </p>

 **Attention :** Ces éléments doivent être invisibles tant que vous n'êtes pas connecté à une carte. Vous allez donc cliquez sur l'arrangement *Fonctionnalités* et décocher le champ *Visible* dans les *Propriétés*. De même, pour le bouton *Déconnexion*. Ce qui vous donnera un résultat suivant : 
 
 <p align = "center"> <img align="center" src="images_tuto_appli/invisible.png " width="700" > </p>

Evidemment, nous ne pouvons pas supprimer les éléments nécessaires à la connexion. Donc, pour obtenir l’apparence que l’on souhaite, il nous faut penser à ce que l’on doit rendre invisible. En effet, lorsque la connexion sera établie, nous n’aurons plus besoin des labels *Conseil_1* et *Conseil_2* ainsi que du bouton *Connection*. 
 
 
 <br />
 
 <h2> 5. Établissement de la connexion et changement d'écran </h2>
 
 Maintenant que cette nouvelle page est créée nous allons programmer ce changement d'écran.
 Ce changement se fera une fois que la connexion est définitivement établie avec le périphérique. 
 
 <li>Ajoutez donc un bloc <code>quand BluetoothLE1 Connected</code>. </li>

Il faut donc cacher les éléments courants (sauf le label *Etat_connexion*) et afficher les éléments de la nouvelle page. 
 
 <u>Changement de page</u>
 
Nous voulons donc rendre invisible les éléments *Conseil_1*, *Conseil_2* et *Connexion*.

<li> Pour chacun de ces composants, utilisez la directive <code>mettre ... Visible à</code> et y imbriquez le champ *Logique* <code>faux</code>.</li>
 
Inversement, nous voulons voir apparaître les éléments liés aux *Fonctionnalités* et le bouton *Déconnexion*. 

<li>Modifiez donc le champ <i>Visible</i> de ces éléments là mais cette fois, imbriquez la valeur logique à <code>vrai</code>.</li>

Ces éléments vont donc devenir visibles.
 
<li>Entre ces deux changements d'états nous allons changer le témoin de connexion <code>Etat_connexion</code> en passant son fond à <i>Vert</i> et son message à "Connexion à .. réussie" comme nous l'avons fait précedemment.</li>
 
 <u>Paramétrage de la carte</u>
 
 Nous allons ensuite paramétrer des <code>UUID</code>. Les UUID sont des clés uniques permettant l'identification entre deux objets BLE. Ce mécanisme est propre à la connexion BLE. 
 Nous allons devoir en définir un certain nombre, un pour mettre en lien le STM32 et le téléphone et d'autre pour gérer l'information (LED, température et autres capteurs). Dans notre cas, nous allons utiliser un capteurs de température et manipuler des LEDs donc nous utiliserons 2 UUID, soit 3 en tout.
 
<li>Il va donc falloir définir 3 variables au tout début du code, comme nous l'avions fait pour <code>global Adresse</code>.</li>

Ces 3 variables s'appellerons <code>Service_UUID</code> et <code>SerialPORT_Temp_UUID</code> (pour le Température) et <code>SerialPORT_LED_UUID</code> (pour les LED). Ces UUID sont des paramètres propres à la carte que vous trouverez dans le code C associé. Les valeurs sont donc les suivantes :
 <p align = "center">
<img align="center" src="images_tuto_appli/UUID.png " width="500" >
</p> 
 
 Le traitement de la température est un peu spécial. En effet, il faut préciser dans le programme que nous souhaitons recevoir des floats provenant de l'UUID *SerialPORT_Temp_UUID*. 
 
 <li>Nous allons donc rajouter la structure <code>appeler BluetoothLE1 RegisterForFloats</code> qui va indiquer que l'on souhaite recevoir des <i>Floats</i> (des réels).</li>
 
 <li>Nous allons maintenant associer au champs <i>serviceUuid</i> celui que nous avons défini précédemment grâce à la structure <code>obtenir global Service_UUID</code>.</li>
 
 <li>Ensuite il faut indiquer quel UUID particulier doit être utilisé (donc celui de la température) : associez donc le champ <i>characteristicUuid</i> à notre varible <i>SerialPORT_Temp_UUID</i>.</li>
 
 <li>Enfin, nous allons juste signifier que nous souhaitons avoir les nombres en entier donc mettre à <i>faux</i> le champ <i>shortFloat</i>. </li>
 
 Finalement, vous devriez obtenir le code suivant :
 <p align = "center">
<img align="center" src="images_tuto_appli/code_cnx2.png " width="600" >
</p>

<br />
 
<h2>6. Implémentation des capteurs</h2>

Nous devons maintenant associer le code qui prend la température et celui qui allume la LED aux composants correspondants.

</br>

<h4> LED </h4>

Etant donné qu'on utilise un *Switch* pour allumer et éteindre la LED, nous allons être attentif au fait que le bouton change ou pas.

<li> Utilisez donc un bloc <code>quand LED Changé</code>.</li>


Dans ce bloc, nous allons distinguer deux cas : si on veut allumer la LED et si on veut l'éteindre. Nous allons donc tester si le *Switch* est enfoncé ou pas et faire un traitement différent en fonction de l'état du *Switch*.

<li> Ajoutez la structure <code>si ... alors ... sinon</code> se trouvant de l'onglet <i>Contrôle</i></li>

Ce que nous allons tester est l'état de notre *Switch*.

</li> Imbriquez le bloc <code>LED On</code> au <i>si</i> du bloc précédent.</li>

<p align="center"><img align="center" src="images_tuto_appli/led.png " width="600" ></p>

<u> Allumer la LED</u>
 
Si le *Switch* est enfoncé, nous allons signifier à la carte que le LED doit être allumée maintenant. 

<li>Pour cela, nous allons écrire à la carte grâce à la structure <code>appeler BluetoothLE1 WriteBytes</code>. </li>

Cette fonction va permettre de désigner à quel capteur parle la carte. 

<li>Comme auparavant, la valeur du <i>service UUID </i> est contenu dans notre variable globale <i>Service_UUID </i> (récupérée grâce à la fonction <code>obtenir global Service_UUID</code>).</li>

<li>La valeur du <i>characteristicUUID</i> est elle contenue dans la variable <i>SerialPORT_LED_UUID</i> que l'on obtient comme précédemment. </li>

<li>On définit le champ <i>signed</i> à <code>vrai</code> (on aurait pu le mettre à faux, cela n'a pas d'incidence sur notre code, cela indique seulement à la carte si les valeurs qu'on lui transmet peuvent être négatives ou pas). <li>

<li>Nous sommes dans le cas où la LED doit être allumée donc on définit la valeur <i>value</i> à <code>1</code> (champ <code>0</code> dans la catégorie <i>Maths</i>, que l'on modifie à 1).</li>

<p align="center"><img align="center" src="images_tuto_appli/led_1.png " width="600" ></p>
 
<u> Éteindre la LED</u>

Inversement, lorsque l'on veut éteindre la LED, on va faire la même chose mais écrire la valeur 0. 

<li>Faites un clic-droit sur le bloc <code>appeler BluetoothLE1 WriteBytes</code> et cliquez sur <i>Dupliquer</i>. Imbriquez ce bloc dupliqué dans le <i>else</i> et modifiez seulement la valeur <code>1</code> par la valeur <code>0</code>. </li>

<p align="center"><img align="center" src="images_tuto_appli/led_2.png " width="600" ></p>

</br>

<h4> Capteur Température </h4>

Le capteur de température va mesurer une valeur et l'envoyer à la carte. Nous devons récupérer cette valeur et l'afficher à l'écran. 

<li>Pour cela nous avons besoin de la structure <code>quand BluetoothLE1 FloatsReceived</code> qui indique que le capteur de température a reçu des données.</li>

<li>Nous allons maintenant modifier notre label <i>Température</i> en utilisant la fonction <code>mettre Température Texte à</code> auquel on associera la valeur contenue dans la variable <i>floatValues</i> grâce à au bloc <code>obtenir floatValues</code> (<i>floatValues</i> contient les valeurs reçues par la carte grâce au capteur de température).</li>

<p align = "center">
<img align="center" src="images_tuto_appli/temperature.png " width="500" >
</p>

Vous devriez obtenir le code suivant : 
 
<p align = "center">
<img align="center" src="images_tuto_appli/led_tmp.png " width="500" >
</p>
 
 <br />
 
 <h2> 7. Déconnexion </h2>

Une fois que l'utilisateur clique sur le bouton <i>Déconnexion</i>, nous devons déconnecter l'application du périphérique et revenir à la page d'accueil.
Comme pour le bouton *Connexion*, la déconnexion se fait en 2 temps : un bloc pour l'appui sur le bouton et un bloc pour la déconnexion du BLE. 

<li>Dans un premier temps il faut utiliser un bloc <code>quand Déconnexion Clic</code>. </li>

<li>Dans ce bloc on appelera simplement la fonction <code> appeler BluetoothLE1 Déconnecter</code>. </li>

<p align = "center">
<img align="center" src="images_tuto_appli/dcnx_1.png " width="500" >
</p>

<li>Le deuxième bloc sera donc construit autour de la structure <code>quand BluetoothLE1 Disconnected </code>. </li>

Dans ce bloc, nous allons :
<li> Changer la couleur de fond de notre témoin de connexion à rouge
<li> Changer le message du témoin à "Aucun appareil connecté"
<li> Rendre visible les <i>Labels</i> <code>Advice</code>, <code>Advice2</code> et le <i>Bouton</i> <code>Connexion</code>
<li> Rendre invisible le <i>Bouton</i> <code>Déconnexion</code> et l'<i>Arrangement vertical</i> <code>Fonctionnalités</code>.

 <p align = "center">
<img align="center" src="images_tuto_appli/dcnx_2.png " width="500" >
</p>