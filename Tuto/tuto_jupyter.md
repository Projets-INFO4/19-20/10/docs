<div align="center"> <img  width="130" src="images_tuto_jupyter/logo_jupyter.png" alt="logo STM" /></div>
<h1>Interfaçage avec Jupyter </h1>

Pour pouvoir visualiser plus efficacement les résultats des différents programmes et pour réaliser des comptes rendus de TP plus facilement nous pouvons utiliser le notebook Jupyter.

Pour pouvoir utiliser ce notebook nous devons simplement ajouter MicroPython à Jupyter et définir le port correspondant à notre carte. 

Sous Windows, ces commandes sont à taper dans l'invite de commande.

<div align="center"> <img  width="530" src="images_tuto_jupyter/cmd.png" alt="cmd windows" /></div>

<h4> Installation du noyau MicroPython</h4>

<ul> 

<li>Installation de Python3 :</br>
<ul> 
<li>Aller sur le site https://www.python.org/downloads/</li>
<li>Télécharger et Installer Python3</li>
<li> Pour vérifier que l'installation s'est faite correctement <code>python --version</code> et <code>pip --version</code></li></ul></li>

<div align="center"> <img width = "730" src="images_tuto_jupyter/python_pip_version.PNG" alt="pip et python versions" /></div></br>

<li> Installation de Jupyter : </br> 
<ul> 
<li></strong> <code>pip install notebook</code></li>
<li>Pour vérifier que tout fonctionne vous pouvez lancer un notebook Jupyter avec la commande <code>jupyter notebook</code>. Vous devriez arriver sur une page comme celle ci dans votre navigateur :</li> </ul></br>
<div align="center"> <img src="images_tuto_jupyter/jupyter.PNG" alt="Accueil Jupyter" /></div>

</br>
<li> Récupération du projet micro python : </br>
<ul> 
<li>Aller sur le site https://github.com/goatchurchprime/jupyter_micropython_kernel.git

<div align="center"> <img src="images_tuto_jupyter/git_clone.PNG" alt="projet git" /></div>

<li>Télécharger le projet git sous format zip et dézipper le 
<li>Mettre le dossier dans Utilisateur </li></ul></br>

<li> Installation de la librairie : </br>
 <code>pip install -e jupyter_micropython_kernel-master</code></li></br>

<li> Installation du noyau dans Jupyter </br>
<code>python -m jupyter_micropython_kernel.install</code></li></br>

<li> Affichage de la liste des noyaux disponibles : </br>
<code>jupyter kernelspec list</code></li> 
</ul>
<div align="center"> <img src="images_tuto_jupyter/kernelspec.PNG" alt="verification kernel" /></div>

<h4>Utilisation du notebook</h4>

<ul> 

<li> Demarrer Jupyter : <code> jupyter notebook</code></li>

<li> En haut à droite cliquer sur <code>New</code> et sélectionner <code>MicroPython-USB</code>

<div align="center"> <img src="images_tuto_jupyter/jupyter_new.PNG" alt="creation notebook jupyter" /></div>


<li> Pour utiliser la carte nous devons préciser le port à utiliser. Cette ligne est à ajouté comme première ligne du notebook : </br>
<code>serialconnect to –port=COM14 –baud=115200</code>
</ul>
Attention, le port COM pourrait changer suivant l'ordinateur. Vous pouvez maitenant exécuter dans ce notebook tous les programmes décrit dans les tutoriels.