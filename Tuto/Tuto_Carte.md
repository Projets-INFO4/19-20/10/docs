<h1> MicroPython et STM32WB55 </h1>

<h4> Tutoriel de mise en place</h4>

<div align="center"><img src="images_tuto_carte/ST.png" width=400 height=300 alt="Logo STMicroelectronics" /></div>


Ce tutoriel est un résumé du tutoriel fourni par STMicroelectronics. Nous allons voir comment connecter la carte NUCLEO-WB55 à l'ordinateur afin qu'elle soit reconnue par celui-ci comme périphérique. Cela nous permettra de pouvoir exécuter des programmes sur la carte et pouvoir envoyer ou recevoir des données (par exemple pour allumer une LED sur la carte ou recevoir la température prise grâce à un capteur situé sur la NUCLEO).

<br />

<h2>Guide de démarrage rapide Windows 10</h2>

<br /> 

<h3> Préparation des cartes NUCLEO-WB55 </h3>

Cette étape est à réaliser par le professeur ayant un accès administrateur sous Windows 10. Il faudra réaliser ces étapes pour chaque carte NUCLEO utilisée en TP.

<br />

<h4>Configuration électronique</h4>

<b>Nous aurons besoin d'un câble USB vers micro USB.</b>

<p>Cette configuration se résume à : </p>
<ol type="1">
	<li> déplacer le cavalier sur USB_MCU</li>
	<li> utiliser le cavalier présent sur SWDIO et le placer sur les Pins 5 et 7 (à la verticale) du connecteur ARDUINO CN7</li>
	<li> connecter un câble micro USB_USER sur le port en dessous de SW4
</ol>

Voici comment doit être configuré le kit :
<div align="center"> <img src="images_tuto_carte/carte_1.png" width=600 height=550 alt="Carte STM32WB55 ou NUCLEO-WB55" /></div>

Ces modifications permettent de configurer le STM32WB55 en mode bootloader. Nous pourrons ainsi mettre à jour le firmware par USB.

<br /> 

<h4> Programmation des cartes </h4> 

<ul>
	<li>Téléchargez sur le Bureau Windows le dossier TP_Python, disponible sur le drive : <a href="https://drive.google.com/open?id=1cLm9pefx_RRuQrEEgmdGOaU1jEGZOQwZ">https://drive.google.com/open?id=1cLm9pefx_RRuQrEEgmdGOaU1jEGZOQwZ</a></li>
</ul>

Ce dossier devra être présent sur chaque poste de TP pour les étudiants. 

<div align="center"><img src="images_tuto_carte/bureau_Windows.png" width=600 height=350 alt="Bureau Windows" /></div>

Voici la liste des fichiers présents dans le dossier : 

<div align="center"><img src="images_tuto_carte/dossier_tp_python.png" width=800 height=350 alt="Bureau Windows" /></div>

<ul>
	<li>Reliez la carte NUCLEO-WB55 avec le câble USB à votre ordinateur. </li>
</ul>

La première étape consiste à installer les drivers USB libres pour communiquer avec la carte STM32WB55. Pour cela, nous utiliserons le logiciel Zadig. 

<br />

<h4> Installation du driver USB libre (Zadig)</h4>

<ul>
	<li>Double-cliquez sur le logiciel "zadig". </li>
</ul>

<div align="center"><img src="images_tuto_carte/autorisation_zadig.png" width=450 height=350 alt="Autorisation Zadig" /></div>

<ul>
	<li>Autorisez l'application à apporter des modifications en cliquant sur le bouton "Oui". </li>
</ul>

Voici l'interface : 

<div align="center"><img src="images_tuto_carte/zadig_1.png" width=650 height=300 alt="Image Zadig 1" /></div>

<ul>
	<li>Allez dans "Options" puis cliquer sur "List All Devices". </li>
</ul>

<div align="center"><img src="images_tuto_carte/zadig_2.png" width=400 height=220 alt="Image Zadig 1" /></div>

<ul>
	<li>Sélectionnez "USB DFU in FS mode" grâce à la liste déroulante. Si cela n'apparaît pas dans la liste déroulante, changez de câble USB (certains câbles ne fonctionnent pas).</li>
</ul>

<div align="center"><img src="images_tuto_carte/zadig_3.png" width=650 height=300 alt="Image Zadig 1" /></div>


<ul>
	<li>Sélectionnez "libusbK (v3.0.7.0)" en utilisant les flèches : </li>
</ul>

<div align="center"><img src="images_tuto_carte/zadig_4.png" width=250 height=60 alt="Image Zadig 1" /></div>

<ul>
	<li>Lancez ensuite l'installation du driver en cliquant sur "Replace Driver". </li>
</ul>

<div align="center"><img src="images_tuto_carte/zadig_5.png" width=650 height=300 alt="Image Zadig 1" /></div>

Nous utilisons désormais la communication USB avec la carte NUCLEO-WB55 sous Windows. 

<ul>
	<li>Quittez le logiciel Zadig. </li>
</ul>


<br />

<h4> Programmation du firmware MicroPython de la carte (dfu-util)</h4>

Il faut maintenant programmer la carte avec le firmware MicroPython. Nous utiliserons pour cela le logiciel **dfu-util**. Cette étape pourrait potentiellement être effectuée par l'étudiant, dans le cas où il faudrait mettre à jour le firmware MicroPython.

<ul>
	<li>Utilisez le logiciel "flash_dfu-util" en double-cliquant dessus.</li>
	<li>Puis, autorisez Windows à exécuter le programme. </li>
</ul>

Une fenêtre de commande Windows s'ouvre (il se peut que ce soit trop rapide pour le voir), veuillez attendre que la programmation se finisse :

<div align="center"><img src="images_tuto_carte/dfu_util_1.png" width=800 height=400 alt="Image dfu-util 1" /></div>

La programmation est maintenant terminée, la fenêtre se ferme.

<ul>
	<li>Vous devez maintenant déplacer le cavalier du connecteur CN7, PIN 5-7 sur SWDIO :</li>
</ul>

<div align="center"><img src="images_tuto_carte/carte_2.png" width=600 height=550 alt="Image carte_2" /></div>

<ul>
	<li>Appuyez sur le bouton Reset, bouton SW4 (au dessus de la) sur le kit de développement.</li>
</ul>

Un message Windows apparaît :

<div align="center"><img src="images_tuto_carte/dfu_util_2.png" width=400 height=120 alt="Image dfu-util 2" /></div>

Ne prenez pas en compte ce message, concernant les problèmes du lecteur. 

Si vous voyez ce message, c'est que l'opération s'est bien passée.

Vous pouvez maintenant communiquer avec le logiciel MicroPython via la liaison USB. 

Regardons les fichiers générés par le système MicroPython avec l'explorateur Windows : 

<ul>
	<li>Ouvrir avec l'explorateur Windows, le périphérique PYBFLASH.</li>
</ul>

<div align="center"><img src="images_tuto_carte/dfu_util_3.png" width=800 height=350 alt="Image dfu-util 3" /></div>

Nous verrons plus tard comment éditer les scripts Python disponibles dans le système de fichier PYBFLASH. 

Tout d'abord nous allons essayer de communiquer avec l'interpréteur de commande Python directement sur le kit de développement NUCLEO-WB55.


<br />

<h3> Installation de l'environnement de programmation</h3>

Ces manipulations devront être maîtrisées par les étudiants afin qu'ils puissent communiquer avec l'interpréteur et éditer des scripts MicroPython sur le kit NUCLEO-WB55.

<br />

<h4> Configuration du terminal de communication</h4>

Maintenant que MicroPython est présent sur le kit NUCLEO-WB55, nous voudrions tester la communication avec Python. Le test consiste à envoyer une commande python et vérifier que l'exécution s'effectue.

La communication s'effectue par USB par le biais d'un port série, nous avons donc besoin d'un logiciel capable d'envoyer les commandes python sous forme de texte à la carte et de recevoir le résultat de l'exécution.

<ul>
	<li>Pour cela, lancez l'utilisateur Puttytel, disponible dans le répertoire TP_Python :</li>
</ul>

<div align="center"><img src="images_tuto_carte/putty_1.png" width=600 height=600 alt="Image Putty 1" /></div>

<ul>
	<li>Sélectionnez l'option "Serial" :</li>
</ul>

<div align="center"><img src="images_tuto_carte/putty_2.png" width=400 height=200 alt="Image Putty 2" /></div>

<ul>
	<li>Configurez les champs "Serial Line" en COM3 et "Speed" à 115200. Puis cliquez sur "Open".</li>
</ul>

Si le message suivant apparaît : 

<div align="center"><img src="images_tuto_carte/putty_3.png" width=300 height=170 alt="Image Putty 3" /></div>

<ul>
	<li>Vérifiez que le kit est bien énuméré sur le bon port COM, voir <b>Vérifier l'attribution du port COM</b>.</li>
</ul>

Sinon, une nouvelle fenêtre s'affiche : 

<div align="center"><img src="images_tuto_carte/putty_4.png" width=700 height=450 alt="Image Putty 4" /></div>

<ul>
	<li>Appuyez sur CTRL+C pour faire apparaître l'interpréteur de commande python :</li>
</ul>

<div align="center"><img src="images_tuto_carte/putty_5.png" width=700 height=450 alt="Image Putty 5" /></div>

<ul>
	<li>Vous pouvez maintenant exécuter des commandes Python. </li>
</ul>

<div align="center"> <i>Example :</i> print("Hello World")</div>
<div align="center"><img src="images_tuto_carte/putty_6.png" width=700 height=450 alt="Image Putty 6" /></div>

Nous avons maintenant terminé la programmation du firmware MicroPython. 

Notre kit de développement NUCLEO-WB55 est désormais prêt à être utilisé avec Python. 

<ul>
	<li>Gardez la fenêtre Putty ouverte, elle vous sera utile pour voir l'exécution des scripts.</li>
</ul>

**NOTE :** L'autocomplétion (appui sur TAB) est disponible lors de la communcation série.

<br />

<h4>Configuration de l'éditeur de script python</h4>

Nous allons voir, dans cette partie, comment mettre en place un environnement de développement MicroPython. 

<br />

<h4> Installation de l'environnement </h4>

<ul>
	<li>Exécutez l'installateur "Portable Python-3.8.0 x64.exe", l'installateur vous demande où extraire l'environnement Python. </li>
	<li>Utilisez le bouton "..." pour sélectionner le dossier TP_Pyton sur votre Bureau. </li>
</ul>

<div align="center"><img src="images_tuto_carte/pyscript_1.png" width=450 height=170 alt="Image PyScript 1" /></div>

Puis attendez la fin de l'extraction des fichiers. 

Nous disposons maintenant d'un environnement de développement Python avec lequel le développeur pourra écrire des scripts MicroPython. 

L'environnement est disponible dans le dossier "TP_Python/Portable Python-3.8.0 x64" :

<div align="center"><img src="images_tuto_carte/pyscript_2.png" width=800 height=350 alt="Image PyScript 2" /></div>

<ul>
	<li>Lancez l'outil "PyScript-Launcher" : </li>
</ul>


<div align="center"><img src="images_tuto_carte/pyscript_3.png" width=1050 height=800 alt="Image PyScript 3" /></div>

Cet outil vous permettra de débugger et d'exécuter vos scripts python.

**Remarque importante :**

**La libraire "pyb", c'est-à-dire le groupement des fonctions micropython d'accès aux périphériques du microcontrôleur, n'est pas disponible lors du débogage des scripts sous Windows.**

**Attention aussi à n'utiliser que les librairies Python-3 portées sous micropython. Par exemple la librairie "numpy" n'est pas encore implémentée officiellement dans MicroPython, il sera donc impossible d'effectuer un 'import numpy' dans un script MicroPython à vocation d'être téléchargé dans la NUCLEO-WB55.**

Voici la liste des libraires utilisables sous MicroPython : 
<a href="http://docs.micropython.org/en/latest/library/index.html#micropython-specific-libraries"> http://docs.micropython.org/en/latest/library/index.html#micropython-specific-libraries</a>

L'éditeur de script python (PyScript) est donc utile pour le test d'algorithme ou la complétion automatique des méthodes python. 

<br />

<h4>Ecrire et exécuter un script MicroPython </h4>

<ul>
	<li>Revenez à l'éditeur de script PyScript-Launcher, puis développez le lecteur PYBFLASH :</li>
</ul>

<div align="center"><img src="images_tuto_carte/pyscript_4.png" width=300 height=200 alt="Image PyScript 4" /></div>

Nous voyons ici 2 fichiers intéressants : *boot.py* et *main.py*.

Le script *boot.py*, à modifier par les utilisateurs avancés, permet d'initialiser MicroPython. Il permet notamment de choisir quel script sera exécuté après le démarrage de MicroPython, par défaut le script *main.py*.

Laissez la configuration initiale pour le moment.

Nous allons nous intéresser au script *main.py*.

<ul>
	<li>Double-cliquez sur celui-ci :</li>
</ul>

<div align="center"><img src="images_tuto_carte/pyscript_5.png" width=1050 height=800 alt="Image PyScript 5" /></div>

Notre premier script va consister à afficher 10 fois le message "MicroPython est génial" avec le numéro du message.

<ul>
	<li>Ecrivez l'algorithme suivant dans l'éditeur de script :</li>
</ul>

<div align="center"><img src="images_tuto_carte/pyscript_6.png" width=720 height=500 alt="Image PyScript 6" /></div>

<ul>
	<li>Exécutez et vérifiez le résultat de celui-ci en appuyant sur  <img src="images_tuto_carte/pyscript_play.png" width=30 height=30 alt="Image PyScript play" /> ou CTRL+F9 :</li>
</ul>

<div align="center"><img src="images_tuto_carte/pyscript_7.png" width=1050 height=300 alt="Image PyScript 7" /></div>

Le script fonctionne bien sous Windows, nous voulons maintenant l'exécuter sur la carte NUCLEO-WB55.

<ul>
	<li>Pour cela sauvegardez le script *main.py* (CTRL+S), puis revenez au terminal Putty. </li>
</ul>

Le script sera exécuté lors d'un redémarrage de MicroPython.

<ul>
	<li>Pour effectuer un redémarrage appuyez sur CTRL+C (pour afficher l'interpréteur ">>>") puis CTRL+D (pour forcer le redémarrage) dans le terminal Putty : </li>
</ul>

<div align="center"><img src="images_tuto_carte/pyscript_8.png" width=700 height=450 alt="Image PyScript 8" /></div>

Notre script s'est exécuté avec succès ! Nous voyons bien notre message s'afficher 10 fois dans le terminal.

Nous sommes désormais prêt à effectuer des commandes MicroPython nous permettant d'accéder aux périphériques du microcontrôleur STM32WB55.

<br />

<h2>Documentation de référence</h2>

<br />

<h3>Introduction</h3>

Ce document est à destination des professeurs de collèges et lycées. Il est décomposé en plusieurs parties et pourra servir de guide pour la mise en place de travaux pratiques.

La première partie de ce document est dédiée à la cible sur laquelle sera exécutée les programmes micropython, c'est le kit de développement STM32WB55.

Enfin, la deuxième partie décrira l'utilisation de micropython avec des exemples d'utilisation du microcontrôleur.

<div align="center"><img src="images_tuto_carte/micropython.png" width=400 height=300 alt="Image PyScript 8" /> <img src="images_tuto_carte/ST.png" width=400 height=300 alt="Logo ST" /> </div>


<br />

<h3>Le Kit de développement STM32WB55</h3>

<br />

<h4>Aperçu de la carte Nucleo STM32WB55</h4>

La carte STM32-NUCLEO est conçue avec les composants nécessaires au démarrage du microcontrôleur.

Les connecteurs d'extensions (ARDUINO, MORPHO) permettent à l'utilisation de connecter des composants électroniques externes au STM32. 

La carte électronique dispose aussi d'un connecteur pour pile CR2032, de 3 boutons (SW1, SW2, SW3), de 3 LEDs (rouge, verte et bleue) et de 2 connecteurs micro-USB.

<div align="center"><i>Voici le schéma bloc du kit NUCLEO-WB55 :</i></div>
<div align="center"><img src="images_tuto_carte/schema_bloc.png" width=800 height=900 alt="Schema bloc" /></div>


<br />

<h4>Description de la carte Nucleo STM32WB55</h4>

<div align="center"><i>Vue de dessus et dessous du kit de développement :</i></div>
<div align="center"><img src="images_tuto_carte/bloc_dessus.png" width=700 height=540 alt="Vue bloc : dessus" /> <img src="images_tuto_carte/bloc_dessous.png" width=650 height=500 alt="Vue bloc : dessous" /></div>

<br />

<h4>PCB Antenna</h4>

<div align="center"><img src="images_tuto_carte/pcb_antenna.png" width=400 height=150 alt="PCB Antenna" /></div>

La *PCB Antenna* dans la partie supérieure de la carte est  l'antenne bluetooth du microcontrôleur STM32WB55. C'est une antenne dite PCB car elle est intégrée directement au circuit de la board. Sa forme complexe est conçue de sorte à optimiser la réception et l'émission des ondes radio pour la fréquence du bluetooth soit 2,4 GHz. 


<br />

<h4>Connecteurs Arduino / Morpho</h4>

Des kits d'extensions peuvent être ajoutés simplement grâce à ces connecteurs normalisés. On pourra, par exemple, brancher le kit Mems Microphone (X-NUCLEO-CCA02M2) sur la carte NUCLEO-WB55 :

<div align="center"><img src="images_tuto_carte/extensions.png" width=800 height=650 alt="Extensions" /></div>


<br />

<h4>USB User</h4>

C'est le port USB qui nous servira à :

<ul>
	<li>Communiquer avec l'interpréteur MycroPython.</li>
	<li>Programmer le système.</li>
	<li>Alimenter le kit de développement</li>
</ul>


<br />

<h4>Socket CR2032</h4>

Une fois le système programmé, il sera possible d'alimenter le kit par une Pile CR2032, afin de rendre le système portable. 


<br />

<h4>STLINK</h4>

C'est l'outil qui permet de programmer le microcontrôleur, nous ne l'utiliserons pas dans ce tutoriel.


<br />

<h4>User LEDs</h4>

Ces LEDs sont accessibles au développeur et permettent d'être activées grâce à MicroPython.

Voir l'exemple **Changer l'état d'une Sortie (GPIO) (Allumer une LED)**.


<br />

<h4> User Push Buttons</h4>

Ces boutons nous serviront à interagir avec l'utilisateur. Ils sont accessibles au développeur et leur état peut être lu grâce à MicroPython.

Voir l'exemple **Récupérer l'état d'un Bouton (GPIO) (Lecture de l'état d'un bouton)**.


<br />

<h3>MicroPython</h3>

MicroPython est un système Python modifié afin d'être intégré dans des microcontrôleurs ayant des ressources plus limités (faible mémoire, faible fréquence CPU, ...).
Comme vu précédemment, MicroPython intègre une librairie qui permet l'accès aux périphériques (GPIO, TIMER, etc) du microcontrôleur cible. 

C'est cette libraire, se nommant "**pyb**", qui fait l'objet de ce chapitre. 

Nous verrons dans un premier temps des fonctionnalités basiques de cette librairie. Nous pourrons grâce à elle, par exemple, interagir avec les boutons ou allumer les LEDs du kit NUCLEO-WB55.


<br />

<h4>Récupérer l'état d'un Bouton (GPIO) (Lecture de l'état d'un bouton)</h4>

Il existe 3 boutons SW1, SW2 et SW3 disponibles pour le développeur : 

<div align="center"><img src="images_tuto_carte/push_buttons.png" width=400 height=150 alt="Boutons utilisateurs" /></div>

<ul>
	<li>Tout d'abord, lancez la console Putty si elle n'est pas déjà ouverte et configurée (<b>Configuration du terminal de communication</b>). </li>
</ul>

Nous allons voir dans cette sous partie comment initialiser une Pin mode "Entrée" et afficher un message lors de l'appui sur un des 3 boutons en utilisant "**pyb.Pin**".

Nous utiliserons la méthode dire de "pulling" afin de demander au système MicroPython l'état de la Pin (1 ou 0).

Pour des raisons électroniques, l'état de la Pin au repos, bouton relâché, est équivalenr à 1 alors que l'état lors d'un appui bouton est 0.

<ul>
	<li>Ouvrez l'éditeur de script et éditez le fichier *main.py* :</li>
</ul>

```
import pyb # Librairie de MicroPython permettant les accès aux périphériques (GPIO, LED, etc)
import time # Librairie permettant de faire des pauses systèmes

print("Les GPIO avec MicroPython c'est facile")

# Initialisation des Pin d'entrées (SW1, SW2, SW3)
sw1 = pyb.Pin('SW1', pyb.Pin.IN)
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2', pyb.Pin.IN)
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw3 = pyb.Pin('SW3', pyb.Pin.IN)
sw3.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

# Initialisation des variables
ancienne_valeur_sw1 = 0
ancienne_valeur_sw2 = 0
ancienne_valeur_sw3 = 0

while 1: # Création d'une boucle infinie
	# le Système s'endorts pendant 300ms
	time.sleep_ms(300)
	#Récupération de l'état des Boutons 1,2,3
	valeur_sw1 = sw1.value()
	valeur_sw2 = sw2.value()
	valeur_sw3 = sw3.value()
	
	#L'état courant est il différent de l'état précédent ?
	if valeur_sw1 != ancienne_valeur_sw1:
    	    if valeur_sw1 == 0:
        	        print("Le bouton 1 (SW1) est appuyé")
    	    else:
        	        print("Le bouton 1 (SW1) est relâché")
    	    ancienne_valeur_sw1 = valeur_sw1
	
	if valeur_sw2 != ancienne_valeur_sw2:
    	    if valeur_sw2 == 0:
        	        print("Le bouton 2 (SW2) est appuyé")
    	    else:
        	        print("Le bouton 2 (SW2) est relâché")
    	    ancienne_valeur_sw2 = valeur_sw2
	
	if valeur_sw3 != ancienne_valeur_sw3:
            if valeur_sw3 == 0:
        	       print("Le bouton 3 (SW3) est appuyé")
    	    else:
        	       print("Le bouton 3 (SW3) est relâché")
    	    ancienne_valeur_sw3 = valeur_sw3
```

<ul>
	<li>Enregistrez le script *main.py* (CTRL+S), puis redémarrez la carte (CTRL+D) sous Putty. </li>
</ul>

Lorsque vous appuyez sur un des 3 boutons (SW1, SW2, SW3), des messages dans la console Putty vous indique l'état des boutons : 

<div align="center"><img src="images_tuto_carte/state_buttons.png" width=600 height=350 alt="Etat boutons" /></div>


<br />

<h4>Changer l'état d'une Sortie (GPIO) (Allumer une LED)</h4>

L'objectif est maintenant d'allumer les LEDs sur le kit de développement : 

<div align="center"><img src="images_tuto_carte/LEDs.png" width=270 height=150 alt="LEDs" /></div>

Voici l'organisation des LEDs par couleur et numéro : 

<ol type="1">
	<li>LED 1 : <span style="color: #0131B4">Bleu</span></li>
	<li>LED 2 : <span style="color: #00561B">Vert</span></li>
	<li>LED 3 : <span style="color: #EE1010">Rouge</span></li>
</ol>

Sous MicroPython, le module **pyb.LED** permet de gérer les LEDs très simplement. 

Dans cette partie, nous voulons réaliser un "chenillard", cette exercice consiste à allumer les LEDs l'une après l'autre, de façon continue. 

<ul>
	<li>En utilisant la méthode précédente et le script suivant, réaliser le chenillard avec MicroPython : </li>
</ul>

```
import pyb
import time

print("Les LEDs avec MicroPython c'est facile")

# Initialisation des LEDs (LED_1, LED_2, LED_3)
led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

# Initialisation du compteur de LED
compteur_de_led = 0

while 1: # Création d'une boucle infinie
    if compteur_de_led == 0:
    	led_bleu.on()
    	led_rouge.off()
    	led_vert.off()
    elif compteur_de_led == 1:
    	led_bleu.off()
    	led_vert.on()
    	led_rouge.off()
    else:
    	led_bleu.off()
    	led_vert.off()
    	led_rouge.on()
	# On veut allumer la prochaine LED à la prochaine itération de la boucle
    compteur_de_led = compteur_de_led + 1
    if compteur_de_led > 2:
    	compteur_de_led = 0
    time.sleep_ms(500) # le Système s'endorts pendant 500ms
```

<br />

<h4>Lecture d'une valeur analogique (ADC)</h4>

Nous aimerions maintenant convertir la valeur analogique (0-3.3V). d'un signal en valeur numérique (0-4095).

Vous pourrez brancher sur A0-5 une valeur analogique compise entre 0-3.3V. Ce connecteur est directement branché à l'ADC du microcontrôleur. Le signal peut donc être converti en signal numérique : 

<div align="center"><img src="images_tuto_carte/ADC.png" width=200 height=200 alt="ADC" /></div>

Pour cette démonstration nous utiliserons un pentotiomètre (10 KOhm) du commerce que nous branchons sur A0, comme cela : 

<div align="center"><img src="images_tuto_carte/A0.png" width=450 height=400 alt="A0" /></div>

N'importe quel référence de potentiomètre 10 K0hm fonctionne pour la démonstration. Voici une référence (PTV09A-4020F-B103) : 

<div align="center"><img src="images_tuto_carte/PTV09A-4020F-B103.png" width=150 height=180 alt="PTV09A-4020F-B103" /></div>

<ul>
	<li>Avec l'aide du script suivant, utilisez la fonction pyb.ADC pour intéragir avec l'ADC du STM32 : </li>
</ul>

```
import pyb
import time

print("L'ADC avec MicroPython c'est facile")
# Initialisation de l'ADC sur la Pin A0
adc_A0 = pyb.ADC(pyb.Pin('A0'))

while 1:
	valeur_numérique = adc_A0.read()
	# Il faut maintenant convertir la valeur numérique par rapport à la tension de référence (3.3V) et
	# le nombre de bits du convertisseur (12 bits - 4096 valeurs)
	valeur_analogique = (valeur_numérique * 3.3) / 4095
	print("La valeur de la tension est :", valeur_analogique, "V")
	# le Système s'endorts pendant 500ms
	time.sleep_ms(500)
```

Vous pouvez lancer le terminal *Putty* et observer la valeur en Volt qui évolue, toutes les 500ms, lorsque vous tournez la potentiomètre. 


<br />

<h4>Ecran LCD OLED (I2C)</h4>

Il est très facile d'utiliser un écran OLED avec MicroPython pour afficher des messages. Nous verrons dans cet exemple comment brancher l'écran LCD en I2C, puis comment piloter l'écran pour envoyer des messages avec MicroPython.

Pour l'exemple, nous utiliserons l'écran monochrome OLED, 192 x 32 pixels, de Adafruit, cependant tous les écrans intégrant le drive SSD1306 sont compatibles.

Voici comment brancher l'écran OLED : 

<div align="center"><img src="images_tuto_carte/OLED.png" width=1050 height=800 alt="OLED" /></div>

Nous aurons besoin du fichier ssd1306.py, que vous pourrez télécharger à cette adresse : 

<a href="https://drive.google.com/open?id=13o-u-4hq3651JiHV-NB2qdiVXesCq_E9"> https://drive.google.com/open?id=13o-u-4hq3651JiHV-NB2qdiVXesCq_E9 </a>

Lorsque vous aurez terminé le téléchargement, il faudra transférer le fichier dans le répertoire du périphérique PYBFLASH. 

<ul>
	<li>Editez maintenant le script *main.py* :</li>
</ul>

```
from machine import Pin, I2C
import ssd1306
from time import sleep

#Initialisation du périphérique I2C
i2c = I2C(scl=Pin('SCL'), sda=Pin('SDA'), freq=100000)

#Paramétrage des caractéristiques de l'écran
largeur_ecran_oled = 128
longueur_ecran_oled = 32
oled = ssd1306.SSD1306_I2C(largeur_ecran_oled, longueur_ecran_oled, i2c)

#Envoi du texte à afficher sur l'écran OLED
oled.text('MicroPython OLED!', 0, 0)
oled.text('	I2C   ', 0, 10)
oled.text('Trop facile !!!', 0, 20)
oled.show()
```


<br />

<h4>Communication sans fils (BLE)</h4>

Dans cette partie, nous verrons comment communiquer en Bluetooth Low Energy (BLE) avec l'application STBLESensor et la carte de développement WB55. 

<ul>
	<li>Installez, ur votre SmartPhone, l'application sur Google Play ou IOS Store </li>
</ul>

<div align="center"><img src="images_tuto_carte/ST_BLE_Sensor.png" width=800 height=400 alt="ST BLE Sensor" /></div>

Voici la description complète des différents services proposés par l'application STBLESensor : 

<a href="https://github.com/STMicroelectronics/STBlueMS_Android"> https://github.com/STMicroelectronics/STBlueMS_Android </a>

<ul>
	<li>Pour communiquer en Bluetooth Low Energy avec MicroPython, il faudra inclure 2 nouveaux fichiers dans le répertoire du disque usb "PYBFLASH" : </li>
</ul>

<ol type="1">
	<li>*ble_advertising.py* : fichier d'aide à la création du message d'advertising</li>
	<li>*ble_sensor.py* : classe permettant la gestion de la connexion BLE</li>
</ol>

<ul>
	<li>Il fadura télécharger les scripts nécessaires à cet exemple ici : <a href="https://drive.google.com/open?id=1FEHVhl9kjQ9dVpkAGZ9cRo8cryLQV2iL"> https://drive.google.com/open?id=1FEHVhl9kjQ9dVpkAGZ9cRo8cryLQV2iL</a> </li>
</ul>

Grâce au fichier *ble_sensor.py*, nous allons pouvoir créer un objet BLE ayant 1 service et 2 caractéristiques. 

*C'est ce fichier qu'il faudra modifier pour changer le profil BLE, si besoin.*

Une fois le script lancé, le kit de développement WB55 se met à émettre des trames BLE, appelé "advertising". Ces messages permettent d'identifier l'objet Bluetooth et de signifier que le périphérique est prêt à être connecté. 

Le nom du périphérique est : "**WB55-MPY**", nous allons le vérifier avec l'application smartphone si la carte WB55 est en émission bluetooth.

<ul>
	<li>Lancez l'application STBLESensor sur votre Smartphone : </li>
</ul>

<div align="center"><img src="images_tuto_carte/app_1.png" width=400 height=700 alt="Screen App 1" /></div>

<ul>
	<li>Appuyez ensuite sur l'icône loupe pour afficher les périphériques BLE environnant :  </li>
</ul>

<div align="center"><img src="images_tuto_carte/app_2.png" width=400 height=700 alt="Screen App 2" /></div>

Dans cet exemple, le profil BLE que nous avons choisi nous permet de simuler un thermomètre et d'allumer ou d'éteindre une LED. La valeur du thermomètre est générée aléatoirement toutes les secondes.

<ul>
	<li>Connectez-vous à la carte de développement en appuyant sur "WB55-MPY" :</li>
</ul>

<div align="center"><img src="images_tuto_carte/app_3.png" width=400 height=700 alt="Screen App 3" /></div>

*La LED bleue de la carte WB55 doit s'allumer lorsqu'elle est connectée à l'application.*

Nous pouvons observer, sur cet écran, l'évolution aléatoire de la température entre 0 et 100 °C.

Il est possible d'afficher la température en mode graphique. 

<ul>
	<li>Pour cela, appuyez sur le bouton Menu <img src="images_tuto_carte/app_4.png" width=30 height=30 alt="Screen App 4" /> :</li>
</ul>

<div align="center"><img src="images_tuto_carte/app_5.png" width=400 height=700 alt="Screen App 5" /></div>

<ul>
	<li>Appuyez maintenant sur <img src="images_tuto_carte/app_6.png" width=150 height=40 alt="Screen App 6" /> :</li>
</ul>

<div align="center"><img src="images_tuto_carte/app_7.png" width=400 height=700 alt="Screen App 7" /></div>

<ul>
	<li>Pour afficher le graphique, appuyez sur <img src="images_tuto_carte/app_8.png" width=30 height=30 alt="Screen App 8" /> :</li>
</ul>

<div align="center"><img src="images_tuto_carte/app_9.png" width=400 height=700 alt="Screen App 9" /></div>

Vous pouvez utiliser le bouton <img src="images_tuto_carte/app_10.png" width=30 height=30 alt="Screen App 10" /> pour modifier les options du graphique, comme la taille de l'axe X ou l'activation du changement automatique de l'échelle en Y.

Nous allons maintenant étudier l'envoi d'une information depuis le SmartPhone vers la plateforme WB55.
Pour cela nous utilisons l'application pour allumer ou éteindre la LED rouge du kit de développement. 

<ul>
	<li>Pour cela appuyez sur le bouton Menu <img src="images_tuto_carte/app_4.png" width=30 height=30 alt="Screen App 4" /> : </li>
</ul>

<div align="center"><img src="images_tuto_carte/app_5.png" width=400 height=700 alt="Screen App 5" /></div>

<ul>
	<li>Choisissez maintenant l'option <img src="images_tuto_carte/app_11.png" width=150 height=40 alt="Screen App 11" /> : </li>
</ul>

<div align="center"><img src="images_tuto_carte/app_12.png"  width=400 height=700 alt="Screen App 12" />    <img src="images_tuto_carte/app_13.png" width=400 height=700 alt="Screen App 13" /></div>

Vous pouvez, sur cet écran, piloter la LED rouge du kit de développement.


<br />

<h2>Documentation</h2>

Site officiel de MicroPython : 
<a href="https://micropython.org/"> https://micropython.org/ </a>

Documentation général sur MicroPython : 
<a href="http://docs.micropython.org/en/latest/"> http://docs.micropython.org/en/latest/ </a>

Exemple d'utilisation de la libraire **pyb** (très utile) : 
*Attenton le pinout est différent de la cartepyb V1.1 :*
<a href="http://docs.micropython.org/en/latest/pyboard/quickref.html#general-board-control"> http://docs.micropython.org/en/latest/pyboard/quickref.html#general-board-control </a>

Code source du projet MicroPython : 
<a href="https://github.com/micropython/micropython"> https://github.com/micropython/micropython</a>

Site officiel de Python 3 : 
<a href="https://www.python.org/"> https://www.python.org/</a>

Documentation techique de la NUCLEO-WB55 :
<a href="https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html"> https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html </a>

Documentation de l'application Android STBLESensor : 
<a href="https://github.com/STMicroelectronics/STBlueMS_Android"> https://github.com/STMicroelectronics/STBlueMS_Android </a>


<br />

<h2>Annexe</h2>

<br />

<h3>Pinout du kit NUCLEO-WB55</h3>


Voici la correspondance des connecteurs avec le numéro des Pins du microcontrôleur : 

<div align="center"><img src="images_tuto_carte/Pins.png" width=600 height=550 alt="Pins" /></div>

<br />

<h3>Description des Pin du STM32</h3>


<div align="center"><img src="images_tuto_carte/tab1.png" width=850 height=300 alt="Tableau 1 " /></div>

<div align="center"><img src="images_tuto_carte/tab2.png" width=850 height=900 alt="Tableau 2" /></div>

<br />

<h3>Vérifier l'attribution du port COM</h3>

Un port COM a dû être créé sur Windows.

<ul>
	<li>Ecrivez "gestionnaire de périphériques" dans la bar de recherche Windows, puis cliquez sur "Ouvrir" : </li>
</ul>

<div align="center"><img src="images_tuto_carte/port_COM.png" width=720 height=620 alt="Port COM" /></div>

Une nouvelle fenêtre s'ouvre : 

<div align="center"><img src="images_tuto_carte/gestion_peripheriques.png"  width=750 height=600 alt="Gestionnaire des peripheriques" /></div>

<ul>
	<li>Relevez le numéro du port COM. Dans l'exemple ci-dessus, le kit NUCLEO est branché sur le port COM3. C'est ce numéro COMx qu'il faudra renter dans Putty.</li>
</ul>

