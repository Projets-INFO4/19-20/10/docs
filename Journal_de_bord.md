**JOURNAL DE BORD**

**SEANCE 1 (20/01) :** 

Choix du projet

**SEANCE 2 (27/01) :** 

Cours Lora/IoT

**SEANCE 3 (03/02) :**

Familiarisation avec la carte Nucleo STM32WB55RG. 
Rencontre avec les IESE qui participent également à ce projet pour qu'ils nous informent de ce qu'ils ont déjà réalisés et ce qu'ils vont / sont en train de faire. Ils nous ont également aidé à prendre en main la carte en nous donnant le dossier que ST leur avait transmis qui contient : 
- la procedure d'installation et 
- les quelques logiciels nécessaires à l'installation. 

Nous avons donc fait les manipulations pour pouvoir flasher la carte et utiliser les programmes tests fournis par ST. 

Les différents systèmes d'exploitations que nous avons utilisés sont Windows et Linux. Nous avons réussi à faire le tutoriel sur ces 2 systèmes d'exploitations. 

Nous avons également donné des retours aux IESE quant aux tutoriels qu'ils ont créés (par exemple pour l'installation des outils sur Linux). 

**SEANCE 4 (10/02) :**

Nous avons commencé par définir les objectifs de notre projet avec M. Donsez : création d'un site avec Jekyll, recherche d'un plugin VSCode / Atom, création d'une application App Inventor (quand le mode BLE sera livré).

Nous avons donc commmencé par l'installation de Jekyll et des outils nécessaires à l'utilisation de Jekyll. Puis nous avons défini avec M. Donsez et les IESE des exemples de template souhaités. Nous avons fini par nous accorder sur un template. 

Nous nous sommes posées beaucoup de questions car nous avons eu du mal à comprendre ce que le plugin VSCode devait faire et quelles fonctionnalités il devait apporter. Après avoir discuté avec les IESE, ils nous ont montré un plugin Atom qu'ils utilisent sur un autre projet et qui doit avoir les même fonctionnalités que le plugin que nous devons trouver / créer. 

Une fois cela plus clair, nous avons commencé à rechercher ce qui existait déjà en terme de plugin sur VSCode pour la gestion du MicroPython et pour pouvoir communiquer avec la carte. Nous avons donc décidé de commencer par ce tuto : 
*https://hbfsrobotics.com/blog/configuring-vs-code-arm-development-stm32cubemx?fbclid=IwAR1_AGDAWC3EEzKaQKgZWquiPPyLdiIAaAIqRGoN9QHqU0bBTTI2SeIWpWg*

Nous avons commencé à installer les logiciels nécessaires.

**SEANCE 5 (17/02) :**

*Partie Jekyll :*
Nous avons recherché des tutos sur l'utiisation de Jekyll : comment démarrer un projet, comment intégrer des images, comment gérer le template, ... Nous avons donc téléchargé le template et créer un projet github associé. 

*Partie VSCode :*
Nous avons poursuivi l'installation des outils du 1er essai. Nous avons été confrontées à une erreur et nous avons donc commencé à chercher d'autres solutions possibles. M. Donsez nous avait parlé de PlatformIO, nous avons donc commencé à nous renseigner là dessus et nous avons trouvé un tuto qui nous permettrait de communiquer avec la carte : *https://docs.platformio.org/en/latest/ide/vscode.html*. 

Nous avons eu un problème d'installation et nous avons donc installer le plugin pendant la période d'interruption pédagogique. 

**SEANCE 6 (02/03) :**

*Partie Jekyll :*
Nous avons donc demander aux IESE quelle organisation ils souhaitaient pour le site, afin de savoir quelles sections créer et qu'est ce qu'il doit y avoir dans ces sections. Nous avons donc créer les onglets principaux : Micropython, Stmduino et Riot (qui sera vide pour l'instant). Nous avons remarqué que les images des fichiers markdown ne s'affiche pas. 

*Partie VSCode :*
Après l'installation du plugin, il fallait comprendre le fichier d'initialisation du projet. Mais c'est à ce moment-là que nous avons eu un problème avec la carte : nous ne pouvions plus modifier le main.py. Mais grâce à Aurélien (des IESE qui sont sur le projet) qui avait déjà eu ce soucis, il avait pu voir avec les personnes de chez ST comment réinitialiser complètement la carte et nous avons pu la récupérer. 

**SEANCE 7 (03/03) :**

*Partie Jekyll :*
Lors de cette séance, nous avons mis en place les onglets Tutoriels. 

*Partie VSCode :*
Nous avons essayé des configurations pour faire en sorte que le plugin fonctionne mais nous n'arrivons toujours pas à communiquer avec la carte. 

**SEANCE 8 (09/03) :**

Soutenance à 15h30. 

Nous repartons avec des conseils : 
*  revoir le rojet git concernant Jekyll pour qu'on l'intègre à gitlab et non à github
*  travailler sur Linux pour le plugin VSCode plutôt que sur Windows (car les ports sont plus facile à manipuler sur Linux que sur Windows)

Nous avons donc fait l'installation de VSCode et du plugin PlatformIO sur Linux. 

**SEANCE 9 (10/03) :**

*Partie Jekyll :*
Nous avons récupéré les articles du site original. Nous n'arrivions pas à basculer le projet du github vers le gitlab. Nous sommes donc allées demander de l'aide à M. Richard qui nous a dit que notre problème venait du fait que notre projet git contenait lui-même un autre projet git (en l'occurence le git du tempalte Jekyll utilisé). Nous avons donc effacé le .git du template et avons créer un projet sur gitlab contenant notre code pour le site.

*Partie VSCode :*
Après discussion avec M. Richard, nous avons compris que le PlatformIO ne conviendrait pas à notre projet. En effet, le plugin PlatformIO permet de lancer un programme en C++ sur la carte et non un programme en MicroPython comme on le souhaite. Nous avons donc convenu qu'il fallait que l'on recherche comment faire un plugin VSCode (et pourquoi pas s'inspirer de PlatformIO). 
Nous avons aussi remarqué que 2 ports USB étaient présents sur la carte : l'un étant le ST-LINK (qui nous permettra de charger les programmes sur la carte) et l'autre étant un USB-User. 

**SEANCE 10 (16/03) :**

La fermeture de Polytech ayant été décrété, nous sommes toutes les 3 rentrées chez nos parents.
Nous avons donc choisi de nous diviser en 2 sous groupes : 
*  Gloria qui s'est jusque là plutôt concentrée sur la partie Jekyll, s'occupera principalement de cette partie, et
*  Manon et Leila s'occuperont de la partie plugin VSCode. 

Une réunion Skype est prévue cette après-midi avec M. Donsez. 
A cette heure ci, Gloria sera dans le train pour rentrer chez elle et Leila est malade (suspicion de Coronavirus...), elle n'est donc pas en capacité de parler. 

Manon sera néanmoins présente pour cette réunion. De plus, nous avons rédigé un rapport écrit que voici : 

```
Malgré le confinement, nous avons mis en place une organisation concernant le projet : 
Gloria s’occupera de la partie Jekyll et
Manon et Leila travaillerons sur le plugin VSCode.

Pour cela, nous allons définir certains objectifs à atteindre. 

Partie Jekyll : 
Nous avons pu basculer le site sur le gricad et il nous faut maintenant intégrer les différents tutos réalisés par les IESE.
Intégrer tous les tutoriels déjà existants et nous assurer qu’ils soient accessibles à des élèves de lycée
Récupérer tous les documents du site précédent et les fusionner avec le site actuel
Personnaliser le template (couleur par exemple) et intégration d’image, de logo, ...

Partie VSCode : 
Nous avons beaucoup échangé avec Mr Richard mardi. Il a étudié avec nous l’application platform io que nous avions intégré à vscode. Nous avons conclu que cette extension pourrait être une solution à notre problème. Mais il n’est pas complètement adapté car le code indiqué est le C++ alors que nous avons besoin de micro python
Mr Richard nous a donc demandé de
Analyser le fonctionnement d’un plug in 
Analyser le code du plug in platform io pour vscode
Identifier les éléments à garder, ceux à modifier et ceux à supprimer 
Identifier les fonction à ajouter
Pour finalement créer notre propre plug in
Actuellement nous avons compris les grands principes de la création d’un plug in, et nous avons avons bien analyser le code existant même si du travail reste à faire
```

Nous sortons donc de cette réunion avec plusieurs objectifs / constats : 
*   le plugin ne sera pas opérationnel avant la fin du projet donc d'après M. Donsez, nous ne devons pas nous concentrer sur ce point là.
*   nous devons nous pencher plus en détails sur l'application à partir d'App Inventor.
*   le site doit contenir une grille (voir dossier Jekyll) montrant une image pour chaque tuto disponible sur le site.

Malheureusement, nous ne disposons que de 2 cartes pour notre trinome et dans la précipitation du futur confinement, nous n'avons pas pu nous organiser. Donc Gloria et Leila ont une carte et Manon n'en a pas. Manon et Leila travailleront donc ensemble (car elles s'occupent toutes les 2 de la partie App Inventor) via l'outil de communication Discord qui permet entre autres de partager son écran afin de voir l'avancement de l'autre.

Au vu de la situation, nous ne respecterons peut être pas toujours les créneaux ADE concernant le projet et nous travaillerons sur d'autres créneaux horaires. 

Nous allons donc dans les prochains jours essayer de concevoir une application basique sur App Inventor (qui permettra de se connecter à la carte).

**SEANCE 11 (17/03) :**

*Partie Jekyll :*
Après la demande de M. Donsez, nous avons réalisé des recherches pour trouver les grid cards souhaités. Une fois celui-ci trouvé, nous allons essayé de comprendre quelles parties du template de la grid card doit fusionner avec notre template à nous. 

*Partie App Inventor :*
Il faut tout d'abord copier les 2 fichiers stockés dans le dossier *Fichiers_nécessaires* sur la carte. Ces fichiers nous ont été fournis par ST, nous ne les toucheront pas dans un premier temps. 

Nous avons donc essayé de réaliser une première version de l'application mais nous obtenons une erreur de connexion sur le téléphone (voir *erreur_essai1.jpg*). Après recherche, nous ne comprenons pas d'où celà vient, donc nous allons essayer autre chose. 

Après ce deuxième essai, nous n'arrivons toujours pas à nous connecter sur la carte et obtenons une erreur sur l'ordinateur (voir *erreur_essai2.png*). Nous allons donc faire de plus amples recherches pour comprendre d'où cela peut il venir.

**SEANCE 11 (19/03) :**

Ce matin, M. Donsez et les IESE ont eu une réunion afin de discuter du projet. Nous avions cours de Traitement du Signal à ce moment là, nous n'avons donc pas pu participer à cette réunion. 

Néanmoins, nous avons pu discuter avec Aurélien qui nous a indiqué que notre erreur sur App Inventor pouvait venir du fait que la carte est équipée du BLE et non du Bluetooth. Il nous a donc donné une extension nécessaire pour pouvoir nous connecter. Il nous a également fourni un fichier App Inventor (*Appli_IESE/Projet_BLE.aia*) sur lequel nos camarades de IESE travaillent et qui permet de se connecter à une carte en BLE. Nous allons donc étudier ce code et approfondir nos recherches dans les prochains jours.  

**SEANCE 12 (21/03) :**

Aujourd'hui, nous avons avancé la partie concernant la connexion BLE à la carte. Après plusieurs essais, nous avons réussi à nous connecter à la carte et avons pu gérer la déconnexion également. Dans le fichier *connexion_BLE.aia* figure la connexion à la carte. 

Nous avons donc intégré la partie concernant la connexion au fichier *BLE_Nucleo.aia* qui sera notre application pour la suite. 
Il faut maintenant que l'on gère la partie démonstration par exemple, allumer une LED ou recevoir la température. 

**SEANCE 13 (22/03) :**

Nous avons réalisé l'allumage et l'éteignage de la LED à distance, cela fonctionne. Nous avons tenté de réaliser le relevé de température, mais des nombres insensés s'écrivait sur l'écran (ce que nous pensions normal, étant donné que nous ne possédons pas le capteur de température). 
Nous avons donc demandé à Aurélien des IESE de tester notre application avec le capteur et ça a fonctionné. 
Nous sortons donc de cette séance avec une application qui allume une LED et reçoit / envoie la température.

**SEANCE 14 (23/03) :**

Nous avons terminé l'intégration des tutoriels des IESE sur le site. Il faut maintenant : 
* intégrer les images au tutoriels et
* merger avec le master sur le gitlab du projet micropython.

Après réunion Skype avec M. Donsez, nous avons donc pour objectif de réaliser un tuto (en markdown) : comment réaliser une application avec App Inventor. 
Mais pour pouvoir connecter la carte, il nous faut avoir réaliser le tuto fourni par ST. Il faut donc, lui aussi, l'écrire en markdown. 

Donc Manon va se charger de commencer le tuto pour la réalisation de l'application et Leila va se charger d'écrire le tuto de ST en markdown. 

**SEANCE 15 (30/03) :**

L'intégration des images sur le site avance, cela prend beaucoup de temps car nous devons modifier chaque lien de chaque tutoriel un par un. 

Nous avons terminé le tuto de ST (fichier *Tuto/tuto_cartre.md*). Nous allons donc essayé de terminer le tuto pour l'application le plus rapidement possible. 

**SEANCE 16 (05/04) :**

Manon a terminé sa partie du tuto. Leila doit maintenant rajouter la sienne.

**SEANCE 17 (06/04) :**

Concernant le site, nous avons réussi à intégrer toutes les images dans les tutos. Nous avons également intégrer nos tutoriels au site.

Le tutoriel concernant l'application App Inventor est terminé. 
Comme vu avec Monsieur Donsez la semaine dernière, nous allons probablement nous concentré sur le point : Interfaçage avec Jupyter via un échange Firmata sur BLE. 
Nous ne comprenons pas bien encore ce qui nous ait demandé, nous allons faire un point avec M. Donsez demain. 

**SEANCE 18 (14/04) :**

M. Donsez a une visio-conférence mercredi 15/04 avec la société Inventys. Nous avons passé le week-end a essayer d'intégrer le site sur le gitlab. Mais nous n'y arrivions pas à cause de problèmes de droits. Nous n'avions pas les droits de pouvoir créer un sous-projet et nous n'avions pas les droits du push sur ce sous-projet. 
Nous avons donc demandé de l'aide à M. Donsez et avons enfin réussi à push et à faire en sorte que le pipeline fonctionne. 

**SEANCE 19 (20/04) :**

Concernant la partie Jekyll, nous avons un petit problème d'image dans le tutoriel d'installation de la carte, mais nous n'arrivons pas à comprendre pourquoi. En effet, lorsque l'on visualise le tutoriel sur le gitlab, nous arrivons à visualiser les images. En revanche, sur le site, elles n'apparaissent pas. Nous allons essayer de régler ce problème dans les prochains jours. 

Avancement du tutoriel Jupyter : nous avons essayé de réaliser un tutoriel à partir de ce que nous avions trouvé sur internet. Mais, étant donné que nous n'avons pas les capteurs, nous avons demandé de l'aide à Aurélien (car il a un capteur de température) pour savoir si notre tutoriel fonctionnait ou pas. 

**SEANCE 20 (24/04) :**

Nous avons terminé le tutoriel Jupyter dans lequel on explique comment ajouter la reconnaissance du langage micropython au notebook Jupyter. Nous ne pouvons pas réaliser de tutoriel sur comment utiliser un notebook avec la carte, car nous ne possédons pas les capteurs nécessaires (vu avec Monsieur Donsez). 

**SEANCE 21 (30/04) :**

Nous avons uploadé la vidéo de présentation et notre rapport. 
