Gloria Ngena - Leila Michelard - Manon Chaix

# Rapport Projet 
#### Support pédagogique open source pour l'initiation à l'internet des objets pour l'enseignement de SNT (Sciences Numériques et Technologie) aux lycées

Nous avons choisi ce projet car c'est un projet de grande envergure, qui nous a permis d'étudier certains sujets que l'on n'abordent pas dans notre spécialité. 
Nous sommes parvenues à remplir les objectifs qui nous étaient demandés. 

Vous trouverez notre journal de bord à l'adresse suivante : [https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/10/docs/-/blob/master/Journal_de_bord.md](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/10/docs/-/blob/master/Journal_de_bord.md)

### Création d'un site web 

Nous sommes parties d'une trame de site déjà existante mais avons adapté la présentation à un template plus approprié pour des tutoriels. Nous l'avons enrichi avec les différents tutos que nos camarades de IESE ont créés et ceux que nous avons nous même conçus. 

Vous trouverez le site web à l'adresse suivante : [https://stm32python.gitlab.io/site/](https://stm32python.gitlab.io/site/)

### Création de tutoriels 

#### Création d'une application et du tutoriel associé

La fonction BLE a été livré en cours de projet nous avons donc pu consacré du temps à la création d'une application permettant de communiquer avec la carte depuis son téléphone. Cette application a pour but de démontrer aux lycéens toutes les fonctionnalités dont dispose la carte. Nous avons utilisé AppInventor car c'est une plateforme très adapté pour ce type d'application, et facile d'utilisation. 
Cela nous a permis de découvrir comment créer simplement une application, chose que nous n'expérimentons pas forcément dans notre formation.

Pour que cette application puisse être réalisé par des étudiants, nous avons créer un tutoriel expliquant chaque étape de réalisation.

#### Création d'un tutoriel pour simplifier l'installation de la carte

STMicroElectronics nous a fourni une doc expliquant comment connecter la carte avec un ordinateur. Nous l'avons simplement retranscrite en markdown en apportant les explications nécessaires pour qu'une fois encore les enseignants et les lycéens y aient accés.

#### Création d'un tutoriel pour utiliser Jupyter

Utiliser Jupyter peut s'avérer très pratique pour recevoir des données et les utiliser pour, par exemple, réaliser des graphiques avec ces données. 
Nous allons donc réaliser un tutoriel sur comment utiliser Jupyter pour recevoir les données envoyées par notre Nucleo WB55.

Vous trouverez les tuto dans le dossier tuto du gricad : [https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/10/docs/-/tree/master/Tuto](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/19-20/10/docs/-/tree/master/Tuto)

Nous sommes heureuses d'avoir pu contribuer à ce projet et espérons qu'il puisse être mené à bien.