**WINDOWS**

Site pour extension VS sur Windows : *https://docs.microsoft.com/fr-fr/visualstudio/extensibility/starting-to-develop-visual-studio-extensions?view=vs-2019*

**LINUX**

Fichiers sources plugin PlatformIO sur VSCode : *https://github.com/platformio/platformio-vscode-ide*
Fichiers sources plugin Pymakr sur VSCode : *https://github.com/pycom/pymakr-vsc*